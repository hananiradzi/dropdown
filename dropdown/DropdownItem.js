import React from 'react';



export default function DropdownItem(props) {
  
  return (
    <a href="#" class="-m-3 p-3 flex items-start rounded-lg hover:bg-gray-50">         
    {props.children}
    <div class="ml-4">
      <p class="text-base font-medium text-gray-900">
        {props.title}
      </p>
      <p class="mt-1 text-sm text-gray-500">
      {props.description}
      </p>
    </div>
  </a>

  );
}